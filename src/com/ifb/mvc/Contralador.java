package com.ifb.mvc;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;

public class Contralador implements ActionListener, WindowListener {
    Modelo modelo;
    Vista vista;

    /**
     * constructor que comunica los visual con la pate logica de la aplicacion
     * @param modelo
     * @param vista
     */
    public Contralador(Modelo modelo, Vista vista) {
        this.modelo = modelo;
        this.vista = vista;
        addlistener(this);
    }

    /**
     * este metodo hace 2 cosas add listener a los botones y cambiarl el actioncomand
     * a los botones
     * @param listener {@link ActionListener}
     */
    private void addlistener(ActionListener listener) {
        //añadir
        //eliminar
        //modificar
        //mafia
        vista.btnMafiaAdd.addActionListener(listener);
        vista.btnMafiaAdd.setActionCommand("addmafia");
        vista.btnMafiaDelete.addActionListener(listener);
        vista.btnMafiaDelete.setActionCommand("deleteMafia");
        vista.btnMafiaModificar.addActionListener(listener);
        vista.btnMafiaModificar.setActionCommand("alterMafia");
        //comisaria
        vista.btnComiisariaAñadir.addActionListener(listener);
        vista.btnComiisariaAñadir.setActionCommand("addcomisaria");
        vista.btnComiisariaEliminar.addActionListener(listener);
        vista.btnComiisariaEliminar.setActionCommand("deletecomisario");
        vista.btnComiisariaModificar.addActionListener(listener);
        vista.btnComiisariaModificar.setActionCommand("altercomisaria");
        //delicuente
        vista.btnDelicuenteAdd.addActionListener(listener);
        vista.btnDelicuenteAdd.setActionCommand("adddelicuente");
        vista.btnDelicuenteEliminar.addActionListener(listener);
        vista.btnDelicuenteEliminar.setActionCommand("deletedelicuente");
        vista.btnDelicuenteModificar.addActionListener(listener);
        vista.btnDelicuenteModificar.setActionCommand("alterDelicuente");
        //policia
        vista.btnPoliciadd.addActionListener(listener);
        vista.btnPoliciaEliminar.addActionListener(listener);
        vista.btnPoliciaModificar.addActionListener(listener);
        vista.btnPoliciadd.setActionCommand("policiaAdd");
        vista.btnPoliciaModificar.setActionCommand("policiamodificar");
        vista.btnPoliciaEliminar.setActionCommand("policiadelete");
        //delito
        vista.btnDelitosadd.addActionListener(listener);
        vista.btnDelitosadd.setActionCommand("addDelito");
        vista.btndelitodelete.addActionListener(listener);
        vista.btndelitodelete.setActionCommand("deleteDelito");
        vista.btndDelitoModificar.addActionListener(listener);
        vista.btndDelitoModificar.setActionCommand("alterDelito");
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String comando=e.getActionCommand();
        switch (comando){
            //mafia
            case "addmafia":{


            }
            break;
            case "deleteMafia":{

            }
            break;
            case "alterMafia":{

            }
            break;
            //comisaria
            case "addcomisaria":{

            }
            break;
            case "altercomisaria":{

            }
            break;
            //delicuente
            case "adddelicuente":{

            }
            break;
            case "deletedelicuente":{

            }
            break;
            case "alterDelicuente":{

            }
            //policia
            break;
            case "policiaAdd":{

            }
            break;
            case "policiamodificar":{

            }
            break;
            case "policiadelete":{

            }
            break;
            //delito
            case "addDelito":{

            }
            break;
            case "deleteDelito":{

            }
            break;
            case "alterDelito":{

            }
            break;
        }
    }

    @Override
    public void windowOpened(WindowEvent e) {

    }

    @Override
    public void windowClosing(WindowEvent e) {

    }

    @Override
    public void windowClosed(WindowEvent e) {

    }

    @Override
    public void windowIconified(WindowEvent e) {

    }

    @Override
    public void windowDeiconified(WindowEvent e) {

    }

    @Override
    public void windowActivated(WindowEvent e) {

    }

    @Override
    public void windowDeactivated(WindowEvent e) {

    }
}
