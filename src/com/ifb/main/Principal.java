package com.ifb.main;

import com.ifb.mvc.Contralador;
import com.ifb.mvc.Modelo;
import com.ifb.mvc.Vista;

/**
 * metodo que ejecuta la applicacion
 */
public class Principal {
    Modelo modelo;
    Vista vista;
    Contralador contralador=new Contralador(modelo,vista);
}
