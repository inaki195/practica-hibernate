package com.ifb.inaki;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Delitos {
    private Integer id;
    private String tipo;
    private String nombre;
    private List<Criminales> delitos;
    private List<Policia> policias;

    @Id
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "tipo")
    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Delitos delitos = (Delitos) o;
        return Objects.equals(id, delitos.id) &&
                Objects.equals(tipo, delitos.tipo) &&
                Objects.equals(nombre, delitos.nombre);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, tipo, nombre);
    }

    @ManyToMany
    @JoinTable(name = "criminal_delito", schema = "ghotam", joinColumns = @JoinColumn(name = "id_delito", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "id_criminal", referencedColumnName = "id"))
    public List<Criminales> getDelitos() {
        return delitos;
    }

    public void setDelitos(List<Criminales> delitos) {
        this.delitos = delitos;
    }

    @ManyToMany
    @JoinTable(name = "delito_policia", schema = "ghotam", joinColumns = @JoinColumn(name = "id_policia", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "id_policia", referencedColumnName = "id"))
    public List<Policia> getPolicias() {
        return policias;
    }

    public void setPolicias(List<Policia> policias) {
        this.policias = policias;
    }
}
