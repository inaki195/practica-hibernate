package com.ifb.inaki;

import javax.persistence.*;
import java.sql.Date;
import java.util.List;
import java.util.Objects;

@Entity
public class Mafia {
    private Integer id;
    private Date fechaCreacion;
    private String nombre;
    private String territorio;
    private List<Criminales> criminales;

    @Id
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "fecha_creacion")
    public Date getFechaCreacion() {
        return fechaCreacion;
    }

    public void setFechaCreacion(Date fechaCreacion) {
        this.fechaCreacion = fechaCreacion;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "territorio")
    public String getTerritorio() {
        return territorio;
    }

    public void setTerritorio(String territorio) {
        this.territorio = territorio;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Mafia mafia = (Mafia) o;
        return Objects.equals(id, mafia.id) &&
                Objects.equals(fechaCreacion, mafia.fechaCreacion) &&
                Objects.equals(nombre, mafia.nombre) &&
                Objects.equals(territorio, mafia.territorio);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, fechaCreacion, nombre, territorio);
    }

    @OneToMany(mappedBy = "mafia")
    public List<Criminales> getCriminales() {
        return criminales;
    }

    public void setCriminales(List<Criminales> criminales) {
        this.criminales = criminales;
    }
}
