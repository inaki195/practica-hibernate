package com.ifb.inaki;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Policia {
    private String nombre;
    private String reputacion;
    private Integer casos;
    private Integer id;
    private Double salario;
    private Comisaria comisaria;
    private List<Delitos> delitos;

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "reputacion")
    public String getReputacion() {
        return reputacion;
    }

    public void setReputacion(String reputacion) {
        this.reputacion = reputacion;
    }

    @Basic
    @Column(name = "casos")
    public Integer getCasos() {
        return casos;
    }

    public void setCasos(Integer casos) {
        this.casos = casos;
    }

    @Id
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "salario")
    public Double getSalario() {
        return salario;
    }

    public void setSalario(Double salario) {
        this.salario = salario;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Policia policia = (Policia) o;
        return Objects.equals(nombre, policia.nombre) &&
                Objects.equals(reputacion, policia.reputacion) &&
                Objects.equals(casos, policia.casos) &&
                Objects.equals(id, policia.id) &&
                Objects.equals(salario, policia.salario);
    }

    @Override
    public int hashCode() {
        return Objects.hash(nombre, reputacion, casos, id, salario);
    }

    @ManyToOne
    @JoinColumn(name = "id_comisaria", referencedColumnName = "id")
    public Comisaria getComisaria() {
        return comisaria;
    }

    public void setComisaria(Comisaria comisaria) {
        this.comisaria = comisaria;
    }

    @ManyToMany(mappedBy = "policias")
    public List<Delitos> getDelitos() {
        return delitos;
    }

    public void setDelitos(List<Delitos> delitos) {
        this.delitos = delitos;
    }
}
