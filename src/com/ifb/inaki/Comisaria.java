package com.ifb.inaki;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Comisaria {
    private Integer id;
    private String nombre;
    private String localizacion;
    private String distrito;
    private List<Policia> polis;

    @Id
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "localizacion")
    public String getLocalizacion() {
        return localizacion;
    }

    public void setLocalizacion(String localizacion) {
        this.localizacion = localizacion;
    }

    @Basic
    @Column(name = "distrito")
    public String getDistrito() {
        return distrito;
    }

    public void setDistrito(String distrito) {
        this.distrito = distrito;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Comisaria comisaria = (Comisaria) o;
        return Objects.equals(id, comisaria.id) &&
                Objects.equals(nombre, comisaria.nombre) &&
                Objects.equals(localizacion, comisaria.localizacion) &&
                Objects.equals(distrito, comisaria.distrito);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, localizacion, distrito);
    }

    @OneToMany(mappedBy = "comisaria")
    public List<Policia> getPolis() {
        return polis;
    }

    public void setPolis(List<Policia> polis) {
        this.polis = polis;
    }
}
