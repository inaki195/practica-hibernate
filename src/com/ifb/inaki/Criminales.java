package com.ifb.inaki;

import javax.persistence.*;
import java.util.List;
import java.util.Objects;

@Entity
public class Criminales {
    private Integer id;
    private String nombre;
    private String apodo;
    private Integer edad;
    private Mafia mafia;
    private List<Delitos> criminales;

    @Id
    @Column(name = "id")
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    @Basic
    @Column(name = "nombre")
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    @Basic
    @Column(name = "apodo")
    public String getApodo() {
        return apodo;
    }

    public void setApodo(String apodo) {
        this.apodo = apodo;
    }

    @Basic
    @Column(name = "edad")
    public Integer getEdad() {
        return edad;
    }

    public void setEdad(Integer edad) {
        this.edad = edad;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Criminales that = (Criminales) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(nombre, that.nombre) &&
                Objects.equals(apodo, that.apodo) &&
                Objects.equals(edad, that.edad);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, nombre, apodo, edad);
    }

    @ManyToOne
    @JoinColumn(name = "id_mafia", referencedColumnName = "id")
    public Mafia getMafia() {
        return mafia;
    }

    public void setMafia(Mafia mafia) {
        this.mafia = mafia;
    }

    @ManyToMany(mappedBy = "delitos")
    public List<Delitos> getCriminales() {
        return criminales;
    }

    public void setCriminales(List<Delitos> criminales) {
        this.criminales = criminales;
    }
}
